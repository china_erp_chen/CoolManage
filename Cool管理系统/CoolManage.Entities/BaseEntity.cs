﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: BaseEntity
// 作    者：CoolHots
// 创建时间：2014/4/20 14:12:10
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CoolManage.Entities
{
    /// <summary>
    /// 模型基类
    /// </summary>
   public class BaseEntity
    {
        private string id;
        [Key]
        [Display(Name = "主键")]
        public string Id
        {
            get
            {
                if (id == null)
                {
                    return Guid.NewGuid().ToString();

                }
                else
                {
                    return id;
                }

            }
            set
            {
                id = value;

            }
        }



        [Display(Name = "状态")]
        [Required(ErrorMessage = "状态不能为空！")]
        public bool Enabled { get; set; }
    }
}
