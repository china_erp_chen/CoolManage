﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: tbModule
// 作    者：CoolHots
// 创建时间：2014/4/20 14:17:47
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CoolManage.Entities
{
    /// <summary>
    /// 系统模块模型
    /// </summary>
    public class tbModule:BaseEntity
    {
        [Display(Name="模块名称")]
        [Required(ErrorMessage="模块名称不能为空")]
        [MaxLength(50,ErrorMessage="模块名称长度不能超过50个字符")]
        public string ModuleName { get; set; }

        [Display(Name="模块链接")]
        [MaxLength(150,ErrorMessage="模块链接长度不能超过150个字符")]
        public string LinkUrl { get; set; }

        [Display(Name="模块图标")]
        [MaxLength(150,ErrorMessage="模块图标长度不能超过150个字符")]
        public string Icon { get; set; }

        [Display(Name="上级模块")]
        [MaxLength(32,ErrorMessage="上级模块编号不能超过32个字符")]
        public string ParentId { get; set; }

        [Display(Name="排序")]
        [Required(ErrorMessage="排序不能为空")]
        public int Sort { get; set; }

        [Display(Name="菜单")]
        [Required(ErrorMessage="菜单选项不能为空")]
        public bool IsMenu { get; set; }

        /// <summary>
        /// 拥有此权限的角色
        /// </summary>
        public virtual ICollection<tbRole> RoleList { get; set; }


    }
}
