﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: tbActionLog
// 作    者：CoolHots
// 创建时间：2014/4/20 15:07:21
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace CoolManage.Entities
{
    /// <summary>
    /// 行为执行日志
    /// </summary>
   public class tbActionLog:BaseEntity
    {
       [DisplayName("执行者IP")]
       [MaxLength(20)]
       public string ActionIP { get; set; }

       [DisplayName("触发行为的表")]
       [MaxLength(50)]
       public string Model { get; set; }

       [DisplayName("日志描述")]
       [MaxLength(200, ErrorMessage = "日志描述长度不能超过30个字符")]
       public string Remark { get; set; }

       [DisplayName("执行时间")]
       public DateTime CreateTime { get; set; }

       /// <summary>
       /// 系统行为
       /// </summary>
       public virtual tbAction Action { get; set; }

       /// <summary>
       /// 执行用户
       /// </summary>
       public virtual tbUser User { get; set; }


    }
}
