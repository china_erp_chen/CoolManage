﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: tbUser
// 作    者：CoolHots
// 创建时间：2014/4/18 20:31:53
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace CoolManage.Entities
{
    public class tbUser : BaseEntity
    {
        /// <summary>
        /// 用户模型
        /// </summary>

        [Display(Name = "用户名")]
        [Required(ErrorMessage = "用户名不能为空！")]
        [MaxLength(50, ErrorMessage = "用户名长度不能超过50个字符！")]
        public string UserName { get; set; }


        [Display(Name = "密码")]
        [Required(ErrorMessage = "密码不能为空！")]
        [MaxLength(50)]
        public string PassWord { get; set; }

        [Display(Name = "密钥")]
        [MaxLength(6)]
        public string Encrypt { get; set; }

        [Display(Name = "手机号码")]
        [MaxLength(50, ErrorMessage = "手机号码长度不能超过18个字符！")]
        public string Phone { get; set; }

        [Display(Name = "昵称")]
        [Required(ErrorMessage = "昵称不能为空！")]
        [MaxLength(50, ErrorMessage = "昵称长度不能超过50个字符！")]
        public string NickName { get; set; }


        [Display(Name = "最后登录时间")]
        public DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// 用户所属的角色
        /// </summary>
        public virtual ICollection<tbRole> RoleList { get; set; }

    }
}
