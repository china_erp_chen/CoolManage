﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: tbRole
// 作    者：CoolHots
// 创建时间：2014/4/18 20:38:32
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace CoolManage.Entities
{
    
    /// <summary>
    /// 角色模型
    /// </summary>
   public class tbRole:BaseEntity
    {

       [Display(Name = "角色名称")]
       [Required(ErrorMessage = "角色名称不能为空！")]
       [MaxLength(50, ErrorMessage = "角色名称长度不能超过50个字符！")]
       public string RoleName { get; set; }

        [Display(Name = "角色描述")]
        [MaxLength(255, ErrorMessage = "角色名称长度不能超过255个字符！")]
       public string Description { get; set; }


       /// <summary>
       /// 角色下面的用户
       /// </summary>
        public virtual ICollection<tbUser> UserList { get; set; }

       /// <summary>
       /// 角色所属的权限
       /// </summary>
        public virtual ICollection<tbModule> ModuleList { get; set; }
    }
}
