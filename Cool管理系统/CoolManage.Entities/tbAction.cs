﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: tbAction
// 作    者：CoolHots
// 创建时间：2014/4/20 15:04:03
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace CoolManage.Entities
{
    /// <summary>
    /// 系统行为模型
    /// </summary>
   public class tbAction:BaseEntity
    {
       [DisplayName("行为标识")]
       [Required(ErrorMessage = "行为标识不能为空")]
       [MaxLength(30,ErrorMessage="行为标识长度不能超过30个字符")]
       public string Name { get; set; }

       [DisplayName("行为名称")]
       [Required(ErrorMessage = "行为名称不能为空")]
       [MaxLength(30, ErrorMessage = "行为名称长度不能超过30个字符")]
       public string Title { get; set; }

       [DisplayName("行为描述")]
       [MaxLength(200, ErrorMessage = "行为描述长度不能超过30个字符")]
       public string Remark { get; set; }

       public virtual ICollection<tbActionLog> ActionLogList { get; set; }
    }
}
