﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: IBaseDao
// 作    者：CoolHots
// 创建时间：2014/4/18 19:21:06
// 描    述：增删改查的公共接口,这个接口定义泛型的增删改查
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolManage.IDao
{
    /// <summary>
    /// 增删改查的公共接口,这个接口定义泛型的增删改查
    /// </summary>
    /// <typeparam name="T">实体</typeparam>
    public interface IBaseDao<T>
    {
        #region 查询普通实现方案(基于Lambda表达式的Where查询)
        /// <summary>
        /// 获取所有Entity
        /// </summary>
        /// <param name="exp">Lambda条件的where</param>
        /// <returns></returns>
        IEnumerable<T> GetAllEntities(Func<T, bool> exp);
        /// <summary>
        /// 计算总个数(分页用到)
        /// </summary>
        /// <param name="exp">Lambda条件的where</param>
        /// <returns></returns>
        int GetEntitiesCount(Func<T, bool> exp);

        /// <summary>
        /// 分页查询(Linq分页方式)
        /// </summary>
        /// <param name="pageNumber">当前页</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderName">lambda排序名称</param>
        /// <param name="sortOrder">排序,升序or降序</param>
        /// <param name="exp">lambda查询条件where</param>
        /// <returns></returns>
        IEnumerable<T> GetEntitiesForPaging(int pageNumber, int pageSize, Func<T, string> orderName, string sortOrder, Func<T, bool> exp);

        /// <summary>
        /// 根据条件查找单个实体
        /// </summary>
        /// <param name="exp">lambda查询条件where</param>
        /// <returns></returns>
        T GetEntity(Func<T, bool> exp);


        #endregion

        

        #region 添删改

        /// <summary>
        /// 插入实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Insert(T entity);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(T entity);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(T entity);


        #endregion
    }
}
