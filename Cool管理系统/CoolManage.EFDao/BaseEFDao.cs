﻿#region 描述
//-----------------------------------------------------------------------------
// 文 件 名: BaseEFDao
// 作    者：CoolHots
// 创建时间：2014/4/18 19:31:38
// 描    述：
// 版    本：
//-----------------------------------------------------------------------------
// 历史更新纪录
//-----------------------------------------------------------------------------
// 版    本：           修改时间：           修改人：          
// 修改内容：
//-----------------------------------------------------------------------------
// Copyright (C) 2013-2014 http://www.coolhots.net
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoolManage.IDao;
using CoolManage.Entities;
using CoolManage.Common;
using System.Data.Entity;
namespace CoolManage.EFDao
{
    public class BaseEFDao<T> : IBaseDao<T> where T : class,new()
    {
        #region 查询普通实现方案(基于Lambda表达式的Where查询)
        /// <summary>
        /// 获取所有Entity
        /// </summary>
        /// <param name="exp">Lambda条件的where</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllEntities(Func<T, bool> exp)
        {

            DbManageContext Entities = new DbManageContext();

            return Entities.Set<T>().Where(exp).ToList();

        }

        /// <summary>
        /// 计算总个数(分页用到)
        /// </summary>
        /// <param name="exp">Lambda条件的where</param>
        /// <returns></returns>
        public int GetEntitiesCount(Func<T, bool> exp)
        {
            DbManageContext Entities = new DbManageContext();
            return Entities.Set<T>().Where(exp).Count();
        }

        /// <summary>
        /// 分页查询(Linq分页方式)
        /// </summary>
        /// <param name="pageNumber">当前页</param>
        /// <param name="pageSize">分页大小</param>
        /// <param name="orderName">lambda排序名称</param>
        /// <param name="sortOrder">排序,asc or desc</param>
        /// <param name="exp">lambda查询条件where</param>
        /// <returns></returns>
        public IEnumerable<T> GetEntitiesForPaging(int pageNumber, int pageSize, Func<T, string> orderName, string sortOrder, Func<T, bool> exp)
        {
            DbManageContext Entities = new DbManageContext();
            if (sortOrder.ToLower() == "asc") //升序排列
            {

                return Entities.Set<T>().Where(exp).OrderBy(orderName).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {

                return Entities.Set<T>().Where(exp).OrderByDescending(orderName).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            }
        }

        /// <summary>
        /// 根据条件查找单个实体
        /// </summary>
        /// <param name="exp">lambda查询条件where</param>
        /// <returns></returns>
        public T GetEntity(Func<T, bool> exp)
        {
            DbManageContext Entities = new DbManageContext();
            return Entities.Set<T>().Where(exp).ToList().First();
        }
        #endregion


        

        #region 添删改

        /// <summary>
        /// 插入实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Insert(T entity)
        {
            DbManageContext Entities = new DbManageContext();
            Entities.Set<T>().Add(entity);
            return Entities.SaveChanges() > 0;
        }

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Update(T entity)
        {
            DbManageContext Entities = new DbManageContext();
            Entities.Set<T>().Attach(entity);
            Entities.Entry<T>(entity).State = EntityState.Modified;
            return Entities.SaveChanges() > 0;
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Delete(T entity)
        {
            DbManageContext Entities = new DbManageContext();
            Entities.Set<T>().Attach(entity);
            Entities.Entry<T>(entity).State = EntityState.Deleted;
            return Entities.SaveChanges() > 0;
        }

        #endregion

    }
}
